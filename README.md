# Week 8 Mini-Project

## Rust Command-Line Tool with Testing

1. Create a New Cargo Project
To create a new Cargo project, use the cargo new command: `cargo new week8`.

2. Add Dependencies
Add the necessary dependencies to your Cargo.toml file:

3. Add your CLI interface and functionality in `main.rs`. My function will filter out the weight of the objects based on the given range.

4. Navigate to your project directory in the terminal and run the command-line tool with `cargo build --release`.

## Sample output and Testing report

1. I tried to filter out the books that have weight between 1.0 and 3.0. The output shows the two books of weight 2.2 and 1.6. The output gives the two books whose weight is within range and contains the word "Novel" in product, which passes the test.

![image](test1.png)

![image](result1.png)

2. I modified the dataset and tried to filter out the books that have weight between 3.0 and 5.0. The output shows the three books of weight 3.5, 3.2, and 5.0. The output gives the three books whose weight is within range and contains the word "2023" in date, which passes the test.

![image](test2.png)

![image](result2.png)