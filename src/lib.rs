use serde::{Deserialize, Serialize};
use rusoto_core::Region;
use rusoto_s3::{S3Client, S3, GetObjectRequest};
use csv::ReaderBuilder;
use std::error::Error;
use tokio::io::AsyncReadExt;

#[derive(Debug, Deserialize, Serialize)]
pub struct Record {
    #[serde(rename = "Date")]
    date: String,
    #[serde(rename = "Product")]
    product: String,
    #[serde(rename = "Weight")]
    weight: f64,
}

pub async fn weight_filter_cli(min: f64, max: f64) -> Result<Vec<Record>, Box<dyn Error>> {
    let s3_client = S3Client::new(Region::UsEast1); 
    let get_req = GetObjectRequest {
        bucket: "week8-zg112".to_string(),
        key: "week8.csv".to_string(), 
        ..Default::default()
    };

    let result = s3_client.get_object(get_req).await.map_err(|e| e.to_string())?;
    let body = result.body.ok_or("Body is empty".to_string())?;
    let mut body_reader = body.into_async_read();
    let mut csv_content = Vec::new();
    body_reader.read_to_end(&mut csv_content).await.map_err(|e| e.to_string())?;

    let mut rdr = ReaderBuilder::new().from_reader(csv_content.as_slice());
    let mut records = Vec::new();
    for result in rdr.deserialize() {
        let record: Record = result.map_err(|e| e.to_string())?;
        if record.weight >= min && record.weight <= max { 
            records.push(record);
        }
    }

    let json = serde_json::to_string(&records)?;
    println!("{}", json);
    Ok(records)
}

pub fn filter_records(records: Vec<Record>, min: f64, max: f64) -> Vec<Record> {
    records.into_iter()
        .filter(|record| record.weight >= min && record.weight <= max) 
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_weight_filter() {
        let records = vec![
            Record { date: "2023-11-01".into(), product: "Science Textbook".into(), weight: 3.5 }, 
            Record { date: "2023-11-05".into(), product: "Novel".into(), weight: 2.2 },            
            Record { date: "2023-11-03".into(), product: "History Textbook".into(), weight: 5.0 }, 
            Record { date: "2023-11-02".into(), product: "Mystery Novel".into(), weight: 1.6 },    
            Record { date: "2023-11-06".into(), product: "Poetry Collection".into(), weight: 0.8 }, 
        ];

        let filtered_records = filter_records(records, 1.0, 3.0); 
        for record in &filtered_records {
            println!("{:?}", record);
        }
        assert_eq!(filtered_records.len(), 2); 
        assert!(filtered_records.iter().all(|r| r.product.contains("Novel"))); 
    }
}
