use clap::{App, Arg};
use week8::weight_filter_cli;
use std::process;

#[tokio::main]
async fn main() {
    let matches = App::new("Weight Filter CLI")
        .version("1.0")
        .author("ZG")
        .about("Filters products by weight range")
        .arg(Arg::with_name("min")
                 .long("min")
                 .value_name("MIN")
                 .help("Minimum weight bound")
                 .takes_value(true)
                 .required(true))
        .arg(Arg::with_name("max")
                 .long("max")
                 .value_name("MAX")
                 .help("Maximum weight bound")
                 .takes_value(true)
                 .required(true))
        .get_matches();

    let min: f64 = matches.value_of("min").unwrap().parse().expect("Minimum weight bound must be a number");
    let max: f64 = matches.value_of("max").unwrap().parse().expect("Maximum weight bound must be a number");

    if let Err(e) = weight_filter_cli(min, max).await {
        eprintln!("Error: {}", e);
        process::exit(1);
    }
}
